package com.meizizi.common.builder;

import io.github.yedaxia.apidocs.Docs;
import io.github.yedaxia.apidocs.DocsConfig;

/**
 * @description:
 * @program: meizizi
 * @author: dowin
 * @date: 2021-03-18 23:39
 **/

public class JapiDocsBuilder {
    private static String projectPath;
    private static String projectName;
    private static String docsPath;
    private static String apiVersion;

    static {
        projectPath = System.getProperty("user.dir") + "/member";
        projectName = projectPath.substring(projectPath.lastIndexOf('/') + 1);
        docsPath = projectPath + "/src/main/resources/public/docs";
        apiVersion = "";
    }

    public static void main(String[] args) {
        DocsConfig config = new DocsConfig();
        // 项目根目录
        config.setProjectPath(projectPath);
        // 项目名称
        config.setProjectName(projectName);
        // 声明该API的版本
        config.setApiVersion(apiVersion);
        // 生成API 文档所在目录
        config.setDocsPath(docsPath);
        // 配置自动生成
        config.setAutoGenerate(Boolean.TRUE);

//        config.addJavaSrcPath(System.getProperty("user.dir") + "/mall/facade" + "/src/main/java/");
//        config.addJavaSrcPath(System.getProperty("user.dir") + "/mall-common" + "/src/main/java/");
        config.addJavaSrcPath(System.getProperty("user.dir") + "/member" + "/src/main/java/");
        // 执行生成文档
        Docs.buildHtmlDocs(config);
    }
}
