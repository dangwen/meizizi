/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.meizizi.common.utils;

import org.apache.http.HttpStatus;

import java.util.HashMap;
import java.util.Map;

/**
 * 返回数据
 *
 * @author Mark sunlightcs@gmail.com
 */
public class R extends HashMap<String, Object> {
	private static final long serialVersionUID = 1L;

	/**
	 * 构造器
	 */
	public R() {
		put("code", 0);
		put("msg", "success");
	}

	/**
	 * 返回失败
	 * @return
	 */
	public static R error() {
		return error(HttpStatus.SC_INTERNAL_SERVER_ERROR, "未知异常，请联系管理员");
	}

	/**
	 * 返回失败
	 * @param msg 失败信息
	 * @return
	 */
	public static R error(String msg) {
		return error(HttpStatus.SC_INTERNAL_SERVER_ERROR, msg);
	}

	/**
	 * 返回失败
	 * @param code 错误码
	 * @param msg 错误信息
	 * @return
	 */
	public static R error(int code, String msg) {
		R r = new R();
		r.put("code", code);
		r.put("msg", msg);
		return r;
	}

	/**
	 * 返回成功
	 * @param msg 返回信息
	 * @return
	 */
	public static R ok(String msg) {
		R r = new R();
		r.put("msg", msg);
		return r;
	}

	/**
	 * 返回成功
	 * @param map 返回信息集合
	 * @return
	 */
	public static R ok(Map<String, Object> map) {
		R r = new R();
		r.putAll(map);
		return r;
	}

	/**
	 * 返回成功
	 * @return
	 */
	public static R ok() {
		return new R();
	}

	/**
	 * put entry
	 * @param key key
	 * @param value value
	 * @return
	 */
	public R put(String key, Object value) {
		super.put(key, value);
		return this;
	}
}
