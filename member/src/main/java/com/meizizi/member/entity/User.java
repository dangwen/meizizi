package com.meizizi.member.entity;

import lombok.Data;

/**
 * @description: 用户类
 * @program: meizizi
 * @author: dove
 * @date: 2021-03-18 23:33
 **/
@Data
public class User {

    /**
     * 用户姓名
     */
    private String name; //用户姓名
    /**
     * 用户地址
     */
    private String addr;
    /**
     * 用户年龄
     */
    private int age;
}
