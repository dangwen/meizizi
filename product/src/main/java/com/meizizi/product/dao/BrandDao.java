package com.meizizi.product.dao;

import com.meizizi.product.entity.BrandEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 品牌
 * 
 * @author dowin
 * @email dowin.dang@easemob.com
 * @date 2021-03-19 00:32:14
 */
@Mapper
public interface BrandDao extends BaseMapper<BrandEntity> {
	
}
