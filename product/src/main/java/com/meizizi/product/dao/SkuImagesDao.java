package com.meizizi.product.dao;

import com.meizizi.product.entity.SkuImagesEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * sku图片
 * 
 * @author dowin
 * @email dowin.dang@easemob.com
 * @date 2021-03-19 00:32:15
 */
@Mapper
public interface SkuImagesDao extends BaseMapper<SkuImagesEntity> {
	
}
