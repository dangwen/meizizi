package com.meizizi.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.meizizi.common.utils.PageUtils;
import com.meizizi.product.entity.AttrGroupEntity;

import java.util.Map;

/**
 * 属性分组
 *
 * @author dowin
 * @email dowin.dang@easemob.com
 * @date 2021-03-19 00:32:14
 */
public interface AttrGroupService extends IService<AttrGroupEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

