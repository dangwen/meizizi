package com.meizizi.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.meizizi.common.utils.PageUtils;
import com.meizizi.product.entity.SkuInfoEntity;

import java.util.Map;

/**
 * sku信息
 *
 * @author dowin
 * @email dowin.dang@easemob.com
 * @date 2021-03-19 00:32:14
 */
public interface SkuInfoService extends IService<SkuInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

