package com.meizizi.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.meizizi.common.utils.PageUtils;
import com.meizizi.product.entity.SpuCommentEntity;

import java.util.Map;

/**
 * 商品评价
 *
 * @author dowin
 * @email dowin.dang@easemob.com
 * @date 2021-03-19 00:32:14
 */
public interface SpuCommentService extends IService<SpuCommentEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

