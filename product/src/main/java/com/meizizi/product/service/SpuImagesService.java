package com.meizizi.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.meizizi.common.utils.PageUtils;
import com.meizizi.product.entity.SpuImagesEntity;

import java.util.Map;

/**
 * spu图片
 *
 * @author dowin
 * @email dowin.dang@easemob.com
 * @date 2021-03-19 00:32:14
 */
public interface SpuImagesService extends IService<SpuImagesEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

