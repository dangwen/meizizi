package com.meizizi.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.meizizi.common.utils.PageUtils;
import com.meizizi.product.entity.SpuInfoDescEntity;

import java.util.Map;

/**
 * spu信息介绍
 *
 * @author dowin
 * @email dowin.dang@easemob.com
 * @date 2021-03-19 00:32:14
 */
public interface SpuInfoDescService extends IService<SpuInfoDescEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

