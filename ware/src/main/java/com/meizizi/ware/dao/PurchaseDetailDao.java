package com.meizizi.ware.dao;

import com.meizizi.ware.entity.PurchaseDetailEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author dowin
 * @email dowin.dang@easemob.com
 * @date 2021-03-19 00:33:41
 */
@Mapper
public interface PurchaseDetailDao extends BaseMapper<PurchaseDetailEntity> {
	
}
